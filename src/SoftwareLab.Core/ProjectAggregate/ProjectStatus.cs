﻿namespace SoftwareLab.Core.ProjectAggregate;

public enum ProjectStatus
{
  InProgress,
  Complete
}
