using MediatR;
using SoftwareLab.Core.Exceptions;
using SoftwareLab.Core.Repositories;

namespace SoftwareLab.Core.Features.User.Query.GetUserById
{
  public class GetUserByIdQuery : IRequest<GetUserByIdViewModel>
  {
    public int Id { get; set; }

    public class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, GetUserByIdViewModel>
    {
      private readonly IUserRepository _userRepository;

      public GetUserByIdQueryHandler(IUserRepository userRepository)
      {
        _userRepository = userRepository;
      }

      public async Task<GetUserByIdViewModel> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
      {
        var user = await _userRepository.GetByIdAsync(request.Id);
        if (
          user == null
        )
        {
          throw new UserNotFoundException("User not found");
        }

        return new GetUserByIdViewModel
        {
          Email = user.Email,
          Name = user.Name
        };
      }
    }
  }
}
