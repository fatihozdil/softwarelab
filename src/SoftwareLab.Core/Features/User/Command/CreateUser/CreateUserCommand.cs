using MediatR;
using SoftwareLab.Core.Exceptions;
using SoftwareLab.Core.Repositories;
using SoftwareLab.Core.Services;

namespace SoftwareLab.Core.Features.User.Command.CreateUser
{
  public class CreateUserCommand : IRequest<int>

  {
    public string Name { get; set; }
    public string Email { get; set; }
    public DateTime BirthDate { get; set; }


    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, int>
    {
      private readonly IUserRepository _userRepository;
      private readonly IEmailService _emailService;

      public CreateUserCommandHandler(IUserRepository userRepository, IEmailService emailService)
      {
        _userRepository = userRepository;
        _emailService = emailService;
      }

      public async Task<int> Handle(CreateUserCommand request, CancellationToken cancellationToken)
      {
        Entities.User user = await _userRepository.GetByEmailAsync(request.Email);
        if (user != null)
        {
          throw new UserAlreadyRegisteredException(request.Email);
        }

        user = new Entities.User { Name = request.Name, Email = request.Email, BirthDate = request.BirthDate, CreatedAt = DateTime.Now, isEnabled = true }
        ;
        await _userRepository.AddAsync(user);

        _emailService.SendMail(request.Email, "Welcome", "Welcome to our app");

        return user.Id;
      }
    }
  }
}
