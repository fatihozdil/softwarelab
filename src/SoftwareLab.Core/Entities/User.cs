namespace SoftwareLab.Core.Entities
{
  public class User
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime BirthDate { get; set; }
    public bool isEnabled { get; set; }
    
  }
}
