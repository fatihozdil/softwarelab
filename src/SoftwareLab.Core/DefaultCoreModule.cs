﻿using Autofac;
using SoftwareLab.Core.Interfaces;
using SoftwareLab.Core.Services;

namespace SoftwareLab.Core;

public class DefaultCoreModule : Module
{
  protected override void Load(ContainerBuilder builder)
  {
    builder.RegisterType<ToDoItemSearchService>()
        .As<IToDoItemSearchService>().InstancePerLifetimeScope();
  }
}
