using SoftwareLab.Core.Entities;

namespace SoftwareLab.Core.Repositories
{
  public interface IUserRepository : IGenericRepository<User>
  {
    Task<User> GetByEmailAsync(string email);
  }
}
