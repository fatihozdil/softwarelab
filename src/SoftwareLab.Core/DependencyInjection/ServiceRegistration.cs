using Microsoft.Extensions.DependencyInjection;

namespace SoftwareLab.Core.DependencyInjection
{
  public static class ServiceRegistration
  {
    public static void AddApplicationCore(this IServiceCollection services)
    {
      services.AddMediatR(cfg =>
      {
        cfg.RegisterServicesFromAssembly(typeof(ServiceRegistration).Assembly);

      });
    }
  }
}
