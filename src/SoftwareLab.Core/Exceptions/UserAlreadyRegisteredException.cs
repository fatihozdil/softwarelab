namespace SoftwareLab.Core.Exceptions
{
    public class UserAlreadyRegisteredException : Exception
    {
        public UserAlreadyRegisteredException(string email) : base($"User with email {email} already registered")
        {
        }
    }
}
