﻿namespace SoftwareLab.Web.Endpoints.ProjectEndpoints;

public record ProjectRecord(int Id, string Name);
