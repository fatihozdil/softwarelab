using Microsoft.Extensions.DependencyInjection;
using SoftwareLab.Core.Services;
using SoftwareLab.Infrastructure.Contexts;
using SoftwareLab.Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using SoftwareLab.Infrastructure.Repositories;
using SoftwareLab.Core.Repositories;

namespace SoftwareLab.Infrastructure.DependencyInjection
{
  public static class ServiceRegistration
  {
    public static void AddInfrastructure(this IServiceCollection services)
    {
      services.AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase("SoftwareLab"));

      services.AddScoped<IEmailService, EmailService>();

      services.AddTransient<IUserRepository, UserRepository>();
      services.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));
    }
  }
}
