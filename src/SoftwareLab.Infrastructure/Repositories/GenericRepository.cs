using SoftwareLab.Core.Repositories;
using SoftwareLab.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;

namespace SoftwareLab.Infrastructure.Repositories
{
  public class GenericRepository<T> : IGenericRepository<T> where T : class
  {
    private readonly ApplicationDbContext _dbContext;

    public GenericRepository(ApplicationDbContext dbContext)
    {
      _dbContext = dbContext;
    }

    public async Task<T> AddAsync(T entity)
    {
      _dbContext.Set<T>().Add(entity);
      await _dbContext.SaveChangesAsync();

      return entity;

    }

    public Task DeleteAsync(T entity)
    {
      throw new NotImplementedException();
    }

    public async Task<IReadOnlyList<T>> GetAllAsync()
    {
      return await _dbContext.Set<T>().ToListAsync();

    }

    public async Task<T> GetByIdAsync(int id)
    {
      return await _dbContext.Set<T>().FindAsync(id);
    }

    public Task<IReadOnlyList<T>> GetPagedResponseAsync(int pageNumber, int pageSize)
    {
      throw new NotImplementedException();
    }

    public Task UpdateAsync(T entity)
    {
      throw new NotImplementedException();
    }
  }
}
