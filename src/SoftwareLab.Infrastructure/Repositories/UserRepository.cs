using Microsoft.EntityFrameworkCore;
using SoftwareLab.Core.Entities;
using SoftwareLab.Core.Repositories;
using SoftwareLab.Infrastructure.Contexts;

namespace SoftwareLab.Infrastructure.Repositories
{
  public class UserRepository : GenericRepository<User>, IUserRepository
  {
    private readonly DbSet<User> _dbSet;
    public UserRepository(ApplicationDbContext dbContext) : base(dbContext)
    {
    _dbSet = dbContext.Set<User>();
    }

    public async Task<User> GetByEmailAsync(string email)
    {
    
      return await _dbSet.SingleOrDefaultAsync(x => x.Email == email);
    }
  }
}
