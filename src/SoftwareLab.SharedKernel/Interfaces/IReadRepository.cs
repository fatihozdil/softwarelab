﻿using Ardalis.Specification;

namespace SoftwareLab.SharedKernel.Interfaces;

public interface IReadRepository<T> : IReadRepositoryBase<T> where T : class, IAggregateRoot
{
}
